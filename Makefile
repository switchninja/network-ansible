# 
# just type 'make'
#
# also be sure to use 'nvi' to edit me, make files are super picky about <tabs>
#

all: info generate diff

info: header

define HEADER
---------------------------------------------------------------------------------
            __                  __  _                              _           
 ___ ___ __/ /____  __ _  ___ _/ /_(_)__  ___    ___ ___ _____  __(_)______ ___
/ _ `/ // / __/ _ \/  ' \/ _ `/ __/ / _ \/ _ \  (_-</ -_) __/ |/ / / __/ -_|_-<
\_,_/\_,_/\__/\___/_/_/_/\_,_/\__/_/\___/_//_/ /___/\__/_/  |___/_/\__/\__/___/


							"Building better worlds."

---------------------------------------------------------------------------------


endef
export HEADER

define DIVIDER
---------------------------------------------------------------------------------
endef
export DIVIDER

divider:
	@echo "$$DIVIDER"
header:
	clear
	@echo "$$HEADER"

cfw: info clean generate_cfw

igw: info clean generate_igw

efw: info clean generate_efw

ecr: info clean generate_ecr

nlf: info clean generate_nlf

tor: info clean generate_tor

obr: info clean generate_obr

vpn: info clean generate_vpn

egw: info clean generate_egw

oob: info clean generate_oob

generate:
	@echo "Executing ansible playbook to generate configurations for all devices in hub sites..."
	@echo
	/usr/bin/ansible-playbook -i inventory assemble.pb.yaml
	@echo
	@echo "Complete, look in the build/ directory for configuration files."
	@echo

generate_cfw:
	@echo "Executing ansible playbook to generate configuration for the cloud firewalls..."
	@echo
	/usr/bin/ansible-playbook -i inventory -l hub_cfw assemble.pb.yaml
	@echo
	@echo "Complete, look in the build/ directory for configuration files."
	@echo

generate_igw:
	@echo "Executing ansible playbook to generate configuration for the internal gateway routers..."
	@echo
	/usr/bin/ansible-playbook -i inventory -l hub_igw assemble.pb.yaml
	@echo
	@echo "Complete, look in the build/ directory for the configuration files."
	@echo

generate_efw:
	@echo "Executing ansible playbook to generate configuration for the external firewalls..."
	@echo
	/usr/bin/ansible-playbook -i inventory -l hub_efw assemble.pb.yaml
	@echo
	@echo "Complete, look in the build/ directory for the configuration files."
	@echo

generate_ecr:
	@echo "Executing ansible playbook to generate configuration for the external core routers..."
	@echo
	/usr/bin/ansible-playbook -i inventory -l hub_ecr assemble.pb.yaml
	@echo
	@echo "Complete, look in the build/ directory for the configuration files."
	@echo 

generate_nlf:
	@echo "Executing ansible playbook to generate configuration for the network leaf switches..."
	@echo
	/usr/bin/ansible-playbook -i inventory -l hub_nlf assemble.pb.yaml
	@echo
	@echo "Complete, look in the build/ directory for the configuration files."
	@echo 

generate_tor:
	@echo "Executing ansible playbook to generate configuration for the top of rack server switches..."
	@echo
	/usr/bin/ansible-playbook -i inventory -l hub_tor assemble.pb.yaml
	@echo
	@echo "Complete, look in the build/ directory for the configuration files."
	@echo 

generate_obr:
	@echo "Executing ansible playbook to generate configuration for the out-of-band network router..."
	@echo
	/usr/bin/ansible-playbook -i inventory -l hub_obr assemble.pb.yaml
	@echo
	@echo "Complete, look in the build/ directory for the configuration files."
	@echo

generate_vpn:
	@echo "Executing ansible playbook to generate configuration for the site-to-site vpn routers..."
	@echo
	/usr/bin/ansible-playbook -i inventory -l hub_vpn assemble.pb.yaml
	@echo 
	@echo "Complete, look in the build/ directory for the configuration files."
	@echo

generate_egw:
	@echo "Executing ansible playbook to generate configuration for the external gateway routers..."
	@echo
	/usr/bin/ansible-playbook -i inventory -l hub_egw assemble.pb.yaml
	@echo
	@echo "Complete, look in the build/ directory for the configuration files."
	@echo 

generate_oob:
	@echo "Executing ansible playbook to generate configuration for the out of band top of rack switches..."
	@echo
	/usr/bin/ansible-playbook -i inventory -l hub_oob assemble.pb.yaml
	@echo
	@echo "Complete, look in the build/ directory for the configuration files."
	@echo

clean:
	@echo "Cleaning out the build directory..."
	@echo
	rm -rf build/*
	rm -f assemble.pb.retry
	@echo
	@echo "Complete."

backup: 
	@echo "Executing ansible playbook to assemble as-is configurations for diffing..."
	@echo
	/usr/bin/ansible-playbook -i inventory assemble.pb.yaml
	mkdir -p build/old
	cp build/*.conf build/old

diff: header
	@echo "Comparing backup configurations with freshly modified ones..."
	@echo "Original config is denoted with the < symbol."
	@echo "New config is denoted with the > symbol."
	@echo
	cd build ; \
	for i in *.conf ; do \
		echo $$i: ; \
		diff old/$$i $$i ; \
		echo ; \
		echo "$$DIVIDER" ; \
		echo ; \
	done
	@echo



	

