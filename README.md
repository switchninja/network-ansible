# network_hubs
network_hubs is an ansible and jinja2 software package that generates configurations for the primary network hub sites.  It is used for hub site turn up and on-going operation.

## usage
1.  first make backups of the existing configurations.

```
    make backup
```

2.  make your edits to whatever yaml variable files you need to change.  See the 'files' section below for more information on the various yamls responsible for defining stuff.  See the 'examples' section below for examples on how to do common tasks.

3.  generate the configurations.

```
    make
```
4.  If you executed step 1 and did make backup, you will now see diff output telling you what configurations were modified.  At this point, you can simply SCP the configuration to the target device:

```
    scp build/east1-net-s01-203.conf crogers@east1-net-s01-203:/var/tmp/
```


5.  ssh to target device, load 'override' the configuration and do a commit check to verify.

```
    crogers@east1-203-tor-001# load override /var/tmp/east1-net-s01-203.conf
    load complete
    {master:0}[edit]
    crogers@east1-203-tor-001# commit check 
    configuration check succeeds

    {master:0}[edit]
    crogers@east1-203-tor-001# show | compare 
<-- output excluded for brevity -->
```

6.  commit the final config once it passes your inspection.

7.  if you want, you can delete everything in the build directory:
```
    make clean
```

## files
### inventory

The inventory file is just named 'inventory' in the top level directory.

The hosts within the inventory file are grouped according to their roles:

```
hub_vpn
hub_egw
hub_ecr
hub_icr
hub_ifw
hub_efw
hub_igw
hub_obr
hub_oob
hub_tor
hub_nlf
hub_cfw
```
### vars/hubs.yaml

Site-wide variables are defined within this file.  It's pretty well documented and should be relatively self-explanatory.

The most commonly edited sections will likely be the 'isps', 'v4downstreams', and 'v6downstreams'.

###### isps

Your network service providers are defined here.  Which edge router gets what isp is determined by the order you put them in. Odds go to 001 and evens go to 002.

This will be the section you edit when you need to depreff or stop sending/receiving prefixes entirely from a given ASN.  Simply follow the examples provided.

###### v[4,6]downstreams

These are the prefixes that will be advertised to the isps that you defined right above.  These are the supernet aggregate prefixes for the downstream remote-office cidr prefixes.

This is where you will also be able to adjust prepending to influence ingress traffic for a given prefix.  See the examples provided on how to do this.

### vars/cloud.yaml

VPC/cloud services are defined within this file.  This includes upstream bgp4 peers , prefix exports, and security policies.  Each new VPC should be given it's very own group.  Note that currently there is just a single 'aws-tgw' group for the amazon transit gateway.  Additional cloud providers could be named something like 'azure-tgw', for an example.

#### routing
##### cloud_upstreams

Define the upstream VPC bgp peers here, and any relevant information.  See the examples provided.

##### exports_v[4,6]

Define the prefixes to advertise to the upstreams defined above.

##### imports_v[4,6]

Define the prefixes to accept from the upstream vpc peers.

#### security

This reads and is defined like a normal firewall security policy.  It should be fairly self explanatory.

All security policies end in an explicit deny and log for session-init.

Application groups are included at the bottom of this file for ease of reference when defining applications to deny/permit.  Just grep around for the service you need if you don't know it.

##### in2out_policies

These are the security policy defines for traffic going FROM ON-PREM TO VPC.  They are applied in the order defined, so if you need to move things around be sure to do so.

##### out2in_policies

These are the security policy defines for traffic going TO ON-PREM FROM VPC.  They are applied in the oder defined, so again, if you need to move things around be sure to do that.

### vars/remote_sites.yaml

Remote IPSEC attached networks are defined here.  The file is fairly well documented and should be self-explanatory.  The only time this file should get modified is when you're turning up or tearing down new remote networks.

Note the interface assignment on the MX multiservices card is determined by the order you define your sites, so if you rearrange things all your interfaces will shift around.

### group_vars/hub_icr.yaml

This is the yaml file for the internal spine switches.  This is where you will add additional ports, 'internal' ebgp peers, and transit vlans.  This file should be relatively well documented.

This is where the umbrella ebgp peering is defined, for example.

### group_vars/hub_nlf.yaml

This is the yaml file for the network leaf switches.  The only real changes you'll make here on any regular basis is to add or remove ports.

Note that the template logic assumes a port with just a single vlan is an access port, so if you want it to be a trunk, be sure to set 'trunk: true'.  This is documented inside the yaml template as well.

### group_vars/hub_tor.yaml

This is the yaml file for the server top of rack switches.  This is likely one of the files you will be spending the most amount of time working with.  This is where you will add/modify/delete ports and their logical ae bundles.

Simply follow the examples already in the yaml file.

### group_vars/hub_oob.yaml

This is the yaml file for the cabinet out of band switches.  This is likely one of the other files you will be spending the most amount of time working with.  Just like with the hub_tor.yaml, this is where you will add/modify/delete ports and their logical ae bundles.

Simply follow the examples already in the yaml file.

### group_vars/hub_obr.yaml

This is the yaml file for the out of band router, that handles all the routing and links to the out of band top of racks.

### group_vars/hub_egw.yaml

This is the yaml file for the exterior gateway edge routers.  This is not where you add isps, new routes, etc, that is handled in hubs.yaml outlined above.

### group_vars/hub_ecr.yaml

This is the yaml file for the external core routers.  There is little to see here.

### group_vars/hub_efw.yaml

This is the yaml file for the external firewalls.  This is where you will define your nats and any security policies you may need.  Follow the examples as documented.

### group_vars/hub_igw.yaml

This is the yaml file for the internal gateway router.  This defines the upstream and downstream cfw peers.  This really shouldn't get modified much, if at all.

### group_vars/hub_vpn.yaml

This is the yaml file for the inter-site mx104 vpn routers.  This shouldn't get modified much.

### group_vars/hub_cfw.yaml

This is the yaml file for the cloud firewalls.  There won't be much to change here once a site is deployed.  Changes for actual vpc related stuff is within the clouds.yaml talked about above.

## Examples

What follows are common tasks and how and what to configure to update configurations to act upon them.

### cloud<->on-prem security policies

Security policies between cloud and on-prem infrastructure are all managed through the vars/cloud.yaml file.

Scroll down to the security policy section.  You will see two sets of policies, an 'in2out' and an 'out2in'.  These handle security rules for on-prem to cloud, and cloud to on-prem, respectively.

The rules are processed in the order you define them.  All policies end in an explicit logging permit that you do not see defined here.

Note at the bottom of this file the pre-defined junos application groups are listed.  This is to aid in rule set creation.

For example, say you wish to allow some hosts in a cloud vpc to access an internal on-prem ms-sql server cluster in west1.  The process would be:

1.  make backup

2.  edit vars/cloud.yaml, scroll down to 'out2in_policies' for whichever site you want to work with.  (east1 or west1 right now)

3.  review rulesets to determine where it needs to be sequenced.

4.  insert or append your ruleset where appropriate, in this instance I would append the following at the very end of the out2in_policies section:

```
vpc:
  aws-tgw-nonprod:
    site: west1
    <-- unrelated removed -->
    #####################
    # SECURITY POLICIES #
    #####################
    <-- on prem to cloud policy cut out -->
    # from cloud to on-prem
    out2in_policies:
    <-- previous policy statements -->
      aws-some_host_in_amazon-to_on-prem_mssql:
        comment: "JIRA-69420 | some comment here about a requestor maybe"
        src:
          - 10.1.1.1/32
          - 10.2.2.2/32
        dst:
          - 172.22.114.11/32
          - 172.22.114.12/32
        protocols:
          - junos-ms-sql
        action: permit
```

The first line is an arbitrary string that is what the rule will be named when the juniper configuration is created.  This is what you will see with show configuration security policies.

The second line is a comment field that is used to store some sort of information such as a jira ticket or requestor information, etc.  This is what will show up inside the juniper /* */ annotations for the ruleset.

The source follows next in ip address format.  Do not use FQDNs or hostnames or anything that isnt an inet4 or inet6 address.  Be sure you include the netmask.  You can have as many entries here as you want.  If you want it to match on anything just use the keyword 'any'.

Destination is next, same rules apply as with the source.

Define the application.  Use your editor's built in search utility to search for 'port 1433' and you'll see the appropriate juniper application name down in the commented section.

Permit or deny.  If you deny it will create a session-init log deny statement.

### modify import/export policies to a given VPC

BGP prefix import and export to our upstream cloud providers is handled through the vars/cloud.yaml file.

Scroll down to the section labeled bgp policies.  You will see imports and exports defined for v4 and v6 prefixes.  Adding to this is as simple as adding another entry to the existing defines. 

For an example, let's say we wish to advertise a new 10.254.254.0/24 and import a new 10.1.0.0/16 from our upstream aws tgw instance in west1.

1.  make backup

2.  edit vars/cloud.yaml, scroll down to 'BGP POLICIES' for west1.  You can't miss it.

3.  append your prefixes in the appropriate locations, so that it looks like this:

```
vpc:
  aws-tgw-nonprod:
    site: west1
    <-- unrelated removed -->
    #####################
    # BGP POLICIES      #
    #####################
    # prefixes to advertise to this vpc
    exports_v4:
      <-- other prefixes -->
      - 10.254.254.0/24
    # prefixes to accept from this vpc
    imports_v4:
      <-- other prefixes -->
      - 10.1.0.0/16
```

### add a new peer to an internet-exchange 

Adding a new peer to an existing IX configuration is super easy.  In this example, let's create a new ipv4 and ipv6 peer to an AS1234567 on the AS1 equinix internet exchange.

1.  make backup

2.  edit vars/hubs.yaml, scroll down to the isp section for AS1, and look for the appropriate upstream number for the IX; in this case 03.  Make it look something like this:

```
hub_sites:
  east1:
    <-- unrelated removed -->
    isps:
      <-- other service providers -->
      03:
        <-- local service provider configuration bits -->
        ix:
          <-- other ASNs -->
          1234567:
            descr: "This is an example autonomous system being configured for display purposes"
            v4peers:
              206.126.239.10:
            v6peers:
              2001:504:0:2:123:4567:1:
```

Note how the individual peer addresses have colons at the end.  This is because there are options you can add to it, like:

```
<-- snip -->
            v4peers:
              206.126.239.10:
                shutdown: true
                depref: true
                import_only: true
                export_only: true
```
The shutdown tag is discussed in more depth in the next section. 

The depref tag will prepend and reduce the local pref of a given peer.  shutdown and depref are mutually exclusive, and shutdown takes precedence- in this example depref would be ignored.

Import_only configures the session to only accept prefixes from the peer, and to not advertise any.

Export_only is the opposite- advertise prefixes only but don't accept any.

### remove an isp or a peer from service

Sometimes you need to remove a peer from service due to various reasons.  This is how you accomplish that.

1.  make backup

2.  edit vars/hubs.yaml, scroll down to the 'isps' section for the given site you which to modify.

In this example, let's assume we want to take the AS1 Zayo circuit out of service.  Simply add the line 'shutdown: true' to the isp definition, so that it looks like this:

```
hub_sites:
  east1:
  <-- unrelated removed -->
    isps:
    <-- unrelated removed -->
      02:
        vlan: 11
        descr: "Zayo CID:IDIA/260135//ZYO"
        asn: 6461
        v4addr: 128.177.78.45/31
        v6addr: 2001:438:fffe::36ea/126
        shutdown: true
```

This will remove all exports and deny all imports from zayo/as6461.

Here's another example, this time we will be shutting down one of the peers going to AS6939 on the seattle internet exchange:

```
hub_sites:
  west1:
  <-- unrelated removed -->
    isps:
    <-- unrelated removed -->
      03:
      <-- unrelated removed -->
        ix:
          6939:
            descr: "AS6939 - Hurricane Electric"
            v4peers:
              206.81.80.40:
                shutdown: true
            v6peers:
              2001:504:16::1b1b:
```
This will remove all exports and deny all imports coming from the 206.81.80.40 peer of as6939.  You can also blanket place the entire ASN into shutdown, which will do all inet and inet6 peers at once, thusly:

```
hub_sites:
  west1:
  <-- unrelated removed --> 
    isps:
    <-- unrelated removed --> 
      03: 
      <-- unrelated removed --> 
        ix: 
          6939:
            shutdown: true
            descr: "AS6939 - Hurricane Electric"
            v4peers:
              206.81.80.40:
            v6peers:
              2001:504:16::1b1b:
```
Note the position of the 'shutdown: true' statement in relation to the previous example.  This will shutdown all inet4 and inet6 peers to a given ASN at once.


### switch port assignments and modifications

Switch ports are set by modifying the related group_vars file, either the hub_tor or the hub_oob.

1.  make backup

2.  edit group_vars/hub_tor.yaml for top of rack switches, or group_vars/hub_oob.yaml for the out of band switches.

In this example I will be creating a new switch port assignment for a host dual homed to east1-net-s01-203:xe-0/0/37 and east1-net-s01-204:xe-0/0/37 with a trunk configuration.

group_vars/hub_tor.yaml:

```
devices:
  east1-net-s01-203:
  <-- unrelated removed -->
    ports:
      <-- other ports not displayed -->
      xe-0/0/37:
        descr: "east1-foo-bar-001:eth0"
        lacp: ae37
      ae37:
        descr: "SERVER: east1-foo-bar-001:bond0 [20Gbps]"
        vlans: 2014 2015 2016 2114
      <-- other ports -->
  east1-net-s01-204:
  <-- unrelated removed -->
    ports:
      <-- other ports not displayed -->
      xe-0/0/37:
        descr: "east1-foo-bar-001:eth1"
        lacp: ae37
      ae37:
        descr: "SERVER: east1-foo-bar-001:bond0 [10Gbps]"
        vlans: 2014 2015 2016 2114
      <-- other ports -->

```
This will create a tengig port at 0/0/37 and assign it to the AE37 lacp bundle, with the vlans 2014,2015,2016, and 2114.  Unfortunately there is currently no support for ranges, so you have to spell them all out.  Since there is more than one vlan assigned here, the template will interpret this as a trunk port.

In the following example I will turn up a switch port to a host needing its ilo port configured, attached to east1-net-oob-203, port 15.

group_vars/hub_oob.yaml:
```
devices:
  east1-net-oob-203:
    <-- unrelated removed -->
    ports:
      <-- other ports -->
      ge-0/0/15:
        descr: "MGMT: east1-foo-bar-001:eth4 [1Gbps]"
        vlans: 2248
      <-- other ports -->
```
This will configure port 15 as an access port in vlan 2248.  The backend logic notices the single vlan and automatically configures it as an access port.  If you wanted it to be a trunk port with just a single vlan, add the tag 'trunk: true' to the port configuration.


### network address translation (NAT) configuration and associated security rules

Network address translations are managed via the group_vars/hub_efw variables file, defined for each cluster.

There is a section for dynamic nats, labeled 'source_nats' and a section for static_nats.

As an example, I will configure a dynamic nat on the east1-net-efw-001 firewall cluster.  Remember nats are processed in the order you enter them, so since this will be a more specific nat, I will place it in front of the default one currently in place.


group_vars/hub_efw.yaml
```
devices:
  east1-net-efw-001:
    <-- unrelated removed -->
    site: east1
    source_nats:
      arbitrary_nat_name:
        comment: "this is a comment field i will use to include a ticket number or something useful"
        src:
          - 172.23.14.0/24
        dst:
          - any
        nat_address:
          - 64.42.188.246/32
          - 192.211.12.246/32
      <-- default rule set begins -->
      default:
      <-- unrelated removed -->
```
This creates a nat under the east1-net-efw-001 firewall cluster called, with the name of 'arbitrary_nat_name', with the specified source and destinations.  I applied two ip addresses to the nat pool, you could just use one if you wanted.  This rule was inserted before the 'default' nat statement in the yaml file.


### internet prefixes, import and export

This is managed via the vars/hubs.yaml file, defined under the 'v4downstreams' and 'v6downstreams' section for each hub site.

Add new lines to the respective section, and assign prepending weighting via a 'primary', 'secondary', or 'tertiary' tag.  These prepends are used to influence the flow of ingress traffic for a prefix that is advertised out of multiple sites.  


### create new user vlan

This is managed via the vars/hubs.yaml file, defined under the 'user_vlans' section towards the bottom of each hub site.

Simply append new lines following the format used:

vars/hubs.yaml:

```
<hubname>:
  user_vlans:
    <vlanID_in_numeric_form>:
      descr: "description of the vlan"
      v4network: <ipv4 subnet>/<subnetmask>
```

note, you don't need to define the v6network, as that is determined by the vlan tag you provide.

### configure new internal user-facing bgp peer

Userland hosts that peer with the core routers are defined and managed via the group_vars/hub_ecr.yaml file, defined under the 'external_peers_v4' and 'external_peers_v6' section of the relavent peering router.

group_vars/hub_icr.yaml:
```
devices:
  <site_icr_a>:
    site: <hub site name>
    external_peers_v4:
      <peer_group_name>:
        asn: <remote asn>
        ip: <remote peer address>
        lip: <local peer address>
        descr: "this is a description of the peer"
        imports:
          # if you dont want to import anything, don't define any import prefixes
          - <inetprefix01>/<netmask>
          - <inetprefix02>/<netmask>
          - <inetprefix03>/<netmask>
        exports:
          # if you dont want to export anything, don't define any export prefixes
          - <inetprefix01>/<netmask>
          - <inetprefix02>/<netmask> 
    external_peers_v6:
      <peer_group_name>:
        asn: <remote asn>
        ip: <remote peer address>
        lip: <local peer address>
        descr: "this is a description of the peer"
        imports:
          # if you dont want to import anything, don't define any import prefixes
          - <inet6prefix01>/<netmask>
        exports:
          # if you dont want to export anything, don't define any export prefixes
          - <inet6prefix01>/<netmask>
```
You can have any number of prefixes for import and export.  If you don't want to import or export anything, then simply leave the 'import' or 'export' sections blank.


### configure new branch office vpns

Branch office vpn connectivity is defined in the vars/remote_sites.yaml file.

New office defines follow the following format:
```
remote_site:
  <three_letter_office_identifier>:
    asn: <office private asn>
    geo: <east | west >
    v4loopback_addr: <loopback inet address of remote office efw/vpn device>
    v6loopback_addr: <loopback inet6 address of remote office efw/vpn device>
    ipv4networks:
      - <office_rfc1918_01>/<subnetmask>
    # note exclude the ipv4publics section entirely if this is a legacy office with no public allocation
    ipv4publics:
      - <office_inet_public_allocation_01>/<subnetmask>
    ipv6networks:
      - <office_ipv6_supernet_01>/<subnetmask>
    ipsec_v4peer01: <remote office isp01 public inet address of efw/vpn device>
    ipsec_v6peer01: <remote office isp01 public inet6 address of efw/vpn device>
    ipsec_v4peer02: <remote office isp02 public inet address of efw/vpn device>
    ipsec_v6peer02: <remote office isp02 public inet6 address of efw/vpn device>
```




## To-Do

Redo the icr peering code to integrate inet/inet6 into a single bgp group.

I still need to include the ability to adjust local-preference for defined prefixes on the import.

We need to stop using the duplicate ip address for the loopback interfaces.  Unfortunately it has been recently discovered that the BFD protocol sometimes gets confused about which loopback interface it's working with and just stops working.  This is a pretty trivial thing to do, but needs ip allocations to first happen.

Everything is statically defined within various yaml templates.  A lot of that, specfically ip address information, should be pulled down dynamically from dcim via api calls.

I'd also like to have route/route6 IRR objects automatically created for the public exports.





## Support

If you require help with any of this, reach out to Christopher Rogers <switchninja@gmail.com>.







