#jinja2: ltrim_blocks: True, lstrip_blocks: True
{% for device_name,device_values in devices.items() %}
    {% if device_name == inventory_hostname %}
        {% for hub_name,hub_values in hub_sites.items() %}
            {% if hub_name == device_values.site %}
                {% set internal_prefix = hub_values.rfc1918_supernet %}
                {% set vlan20prefix = hub_values.v20prefix_v4 %}
                {% set vlan21prefix = hub_values.v21prefix_v4 %}
                {% set vlan20tuple_list = vlan20prefix.split('.') %}
                {% set vlan21tuple_list = vlan21prefix.split('.') %}
                {% set v6supernet = hub_values.v6_supernet %}
                {% set v4tuple_list = internal_prefix.split('.') %}
                {% set v6tuple_list = v6supernet.split(':') %}
                {% set cluster_id = device_name.split('-')[3] %}
                {% if ( device_values.a_management_v4 is defined ) %}
                    {% set a_management_v4 = device_values.a_management_v4 %}
                {% elif ( device_values.a_management_v4 is not defined ) and ( cluster_id == "001" ) %}
                    {% set a_management_v4 = v4tuple_list[0] ~ "." ~ v4tuple_list[1] ~ ".253.236" %}
                {% else %}
                    {% set a_management_v4 = "a_management_v4 NOT DEFINED" %}
                {% endif %}
                {% if ( device_values.b_management_v4 is defined ) %}
                    {% set b_management_v4 = device_values.b_management_v4 %}
                {% elif ( device_values.b_management_v4 is not defined ) and ( cluster_id == "001" ) %}
                    {% set b_management_v4 = v4tuple_list[0] ~ "." ~ v4tuple_list[1] ~ ".253.237" %}
                {% else %}
                    {% set b_management_v4 = "b_management_v4 NOT DEFINED" %}
                {% endif %}
                {% if ( device_values.a_management_v6 is defined ) %}
                    {% set a_management_v6 = device_values.a_management_v6 %}
                    {% set a_management_fe80 = "fe80:2253::" ~ a_management_v6.split('::')[1] %}
                {% elif ( device_values.a_management_v6 is not defined ) and ( cluster_id == "001" ) %}
                    {% set a_management_v6 = v6tuple_list[0] ~ "." ~ v6tuple_list[1] ~ v6tuple_list[2] ~ ":2253::236" %}
                    {% set a_management_fe80 = "fe80:2253::" ~ a_management_v6.split('::')[1] %}
                {% else %}
                    {% set a_management_v6 = "a_management_v6 NOT DEFINED" %}
                    {% set a_management_fe80 = "a_management_fe80 NOT DEFINED" %}
                {% endif %}
                {% if ( device_values.b_management_v6 is defined ) %}
                    {% set b_management_v6 = device_values.b_management_v6 %}
                    {% set b_management_fe80 = "fe80:2253::" ~ b_management_v6.split('::')[1] %}
                {% elif ( device_values.b_management_v6 is not defined ) and ( cluster_id == "001" ) %}
                    {% set b_management_v6 = v6tuple_list[0] ~ "." ~ v6tuple_list[1] ~ v6tuple_list[2] ~ ":2253::237" %}
                    {% set b_management_fe80 = "fe80:2253::" ~ b_management_v6.split('::')[1] %}
                {% else %}
                    {% set b_management_v6 = "b_management_v6 NOT DEFINED" %}
                    {% set b_management_fe80 = "b_management_fe80 NOT DEFINED" %}
                {% endif %}
                {% if ( device_values.v4loopback_addr is not defined ) and ( cluster_id == "001" ) %}
                    {% set loopback_v4 = v4tuple_list[0] ~ "." ~ v4tuple_list[1] ~ ".254.81" %}
                {% elif ( device_values.v4loopback_addr is defined ) %}
                    {% set loopback_v4 = device_values.v4loopback_addr %}
                {% else %}
                    {% set loopback_v4 = "loopback_v4 NOT DEFINED" %}
                {% endif %}
                {% if ( device_values.v6loopback_addr is not defined ) and ( cluster_id == "001" ) %}
                    {% set loopback_v6 = v6tuple_list[0] ~ ":" ~ v6tuple_list[1] ~ ":" ~ v6tuple_list[2] ~ ":ffff::81" %}
                {% elif ( device_values.v6loopback_addr is defined ) %}
                    {% set loopback_v6 = device_values.v6loopback_addr %}
                {% else %}
                    {% set loopback_v6 = "loopback_v6 NOT DEFINED" %}
                {% endif %}
                {% if ( device_values.netflow_exporter is not defined ) and ( cluster_id == "001" ) %}
                    {% set netflow = v4tuple_list[0] ~ "." ~ v4tuple_list[1] ~ ".253.238" %}
                {% elif ( device_values.netflow_exporter is defined ) %}
                    {% set netflow = device_values.netflow_exporter %}
                {% else %}
                    {% set netflow = "netflow NOT DEFINED" %}
                {% endif %}
                {% if device_values.management_default_v4 is not defined %}
                    {% set manage_def_v4 = v4tuple_list[0] ~ "." ~ v4tuple_list[1] ~ ".253.1" %}
                {% else %}
                    {% set manage_def_v4 = device_values.management_default_v4 %}
                {% endif %}
                {% if device_values.management_default_v6 is not defined %}
                    {% set manage_def_v6 = v6tuple_list[0] ~ ":" ~ v6tuple_list[1] ~ ":" ~ v6tuple_list[2] ~ ":2253::1" %}
                {% else %}
                    {% set manage_def_v6 = device_values.management_default_v6 %}
                {% endif %}
                {% if ( device_values.vlan200v4 is not defined ) and ( cluster_id == "001" ) %}
                    {% set vlan200v4 = v4tuple_list[0] ~ "." ~ v4tuple_list[1] ~ ".0.1" %}
                {% elif ( device_values.vlan200v4 is defined ) %}
                    {% set vlan200v4 = device_values.vlan200v4 %}
                {% else %}
                    {% set vlan200v4 = "vlan200v4 NOT DEFINED" %}
                {% endif %}
                {% if ( device_values.vlan200v6 is not defined ) and ( cluster_id == "001" ) %}
                    {% set vlan200v6 = v6tuple_list[0] ~ ":" ~ v6tuple_list[1] ~ ":" ~ v6tuple_list[2] ~ ":200::1" %}
                    {% set vlan200fe80 = "fe80:200::" ~ vlan200v6.split('::')[1] %}
                {% elif ( device_values.vlan200v6 is defined ) %}
                    {% set vlan200v6 = device_values.vlan200v6 %}
                    {% set vlan200fe80 = "fe80:200::" ~ vlan200v6.split('::')[1] %}
                {% else %}
                    {% set vlan200v6 = "vlan200v6 NOT DEFINED" %}
                    {% set vlan200fe80 = "vlan200fe80 NOT DEFINED" %}
                {% endif %}
                {% if ( device_values.vlan20v4 is not defined ) and ( cluster_id == "001" ) %}
                    {% set vlan20v4 = vlan20tuple_list[0] ~ "." vlan20tuple_list[1] ~ "." ~ vlan20tuple_list[2] ~ ".254" %}
                {% elif ( device_values.vlan20v4 is defined ) %}
                    {% set vlan20v4 = device_values.vlan20v4 %}
                {% else %}
                    {% set vlan20v4 = "vlan20v4 NOT DEFINED" %}
                {% endif %}
                {% if ( device_values.vlan21v4 is not defined ) and ( cluster_id == "001" ) %}
                    {% set vlan21v4 = vlan21tuple_list[0] ~ "." vlan21tuple_list[1] ~ "." ~ vlan21tuple_list[2] ~ ".254" %}
                {% elif ( device_values.vlan21v4 is defined ) %}
                    {% set vlan21v4 = device_values.vlan21v4 %}
                {% else %}
                    {% set vlan21v4 = "vlan21v4 NOT DEFINED" %}
                {% endif %}
                {% if ( device_values.vlan20v6 is not defined ) and ( cluster_id == "001" ) %}
                    {% set vlan20v6 = v6tuple_list[0] ~ ":" ~ v6tuple_list[1] ~ ":" ~ v6tuple_list[2] ~ ":20::254" %}
                    {% set vlan20fe80 = "fe80:20::254" %}
                {% elif ( device_values.vlan20v6 is defined ) %}
                    {% set vlan20v6 = device_values.vlan20v6 %}
                    {% set vlan20fe80 = "fe80:20::" ~ vlan20v6.split("::")[1] %}
                {% else %}
                    {% set vlan20v6 = "vlan20v6 NOT DEFINED" %}
                    {% set vlan20fe80 = "vlan20fe80 NOT DEFINED" %}
                {% endif %}
                {% if ( device_values.vlan21v6 is not defined ) and ( cluster_id == "001" ) %}
                    {% set vlan21v6 = v6tuple_list[0] ~ ":" ~ v6tuple_list[1] ~ ":" ~ v6tuple_list[2] ~ ":21::254" %}
                    {% set vlan21fe80 = "fe80:21::254" %}
                {% elif ( device_values.vlan21v6 is defined ) %}
                    {% set vlan21v6 = device_values.vlan20v6 %}
                    {% set vlan21fe80 = "fe80:21::" ~ vlan21v6.split("::")[1] %}
                {% else %}
                    {% set vlan21v6 = "vlan21v6 NOT DEFINED" %}
                    {% set vlan21fe80 = "vlan21fe80 NOT DEFINED" %}
                {% endif %}

-------------------------
variables and their values:

a_management_v4 is {{ a_management_v4 }};
b_management_v4 is {{ b_management_v4 }};
a_management_v6 is {{ a_management_v6 }};
b_management_v6 is {{ b_management_v6 }};
a_management_fe80 is {{ a_management_fe80 }};
b_management_fe80 is {{ b_management_fe80 }};
loopback_v4 is {{ loopback_v4 }};
loopback_v6 is {{ loopback_v6 }};
netflow is {{ netflow }};
manage_def_v4 is {{ manage_def_v4 }};
manage_def_v6 is {{ manage_def_v6 }};
vlan200v4 is {{ vlan200v4 }};
vlan200v6 is {{ vlan200v6 }};
vlan200fe80 is {{ vlan200fe80 }};
vlan20v4 is {{ vlan20v4 }};
vlan21v4 is {{ vlan21v4 }};
vlan20v6 is {{ vlan20v6 }};
vlan20fe80 is {{ vlan20fe80 }};
vlan21v6 is {{ vlan21v6 }};
vlan21fe80 is {{ vlan21fe80 }};


-------------------------
replace:



            {% endif %}
        {% endfor %}
    {% endif %}
{% endfor %}
