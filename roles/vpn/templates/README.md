# variables available for definition
these variables are to be defined as noted.

```
hub_name - REQUIRED.  name as defined in the hub_vpn group vars file under hub_sites section
site_name - REQUIRED.  site name as defined in the hub_vpn group vars file under the devices section

management_v4 - optional.  defined in group/hub_vpn under devices
management_v6 - optional.  defined in group/hub_vpn under devices
management_fe80 - optional.  defined in group/hub_vpn under devices

v4loopback_addr - optional.  defined in group/hub_vpn under devices
v6loopback_addr - optional.  defined in group/hub_vpn under devices

netflow_exporter - optional.  netflow source ip, defined in group/hub_vpn under devices

management_default_v4 - optional.  default virtual-router default inet gateway, defined in group/hub_vpn under devices
management_default_v6 - optional.  default virtual-router default inet6 gateway, defined in group/hub_vpn under devices

public_v4 - optional.  public ip of vpn router on either vlan20 or vlan21, defined in group/hub_vpn under devices.
public_v6 - optional.  public ip6 of vpn router on either vlan20 or vlan21, defined in group/hub_vpn under devices.

vlan200v4 - optional.  internal vlan200 ip, defined in group/hub_vpn under devices.
vlan200v6 - optional.  internal vlan200 ip6, defined in group/hub_vpn under devices.

vlan20fe80 - optional.  vlan20 inet6 link local address, defined in group/hub_vpn under devices.
vlan21fe80 - optional.  vlan21 inet6 link local address, defined in group/hub_vpn under devices.
vlan200fe80 - optional.  vlan200 inet6 link local address, defined in group/hub_vpn under devices.

icrbgpv4_peer01 - optional.  icr-01a inet4 bgp peering address, defined in group/hub_vpn under devices.
icrbgpv4_peer02 - optional.  icr-01b inet4 bgp peering address, defined in group/hub_vpn under devices.

icrbgpv6_peer01 - optional.  icr-01a inet6 bgp peering address, defined in group/hub_vpn under devices.
icrbgpv6_peer02 - optional.  icr-01a inet6 bgp peering address, defined in group/hub_vpn under devices.

ecrbgpv4_peer01 - optional.  ecr-01a inet4 bgp peering address, defined in group/hub_vpn under devices.
ecrbgpv4_peer02 - optional.  ecr-01b inet4 bgp peering address, defined in group/hub_vpn under devices.

ecrbgpv6_peer01 - optional.  ecr-01a inet6 bgp peering address, defined in group/hub_vpn under devices.
ecrbgpv6_peer02 - optional.  ecr-01b inet6 bgp peering address, defined in group/hub_vpn under devices.
```

# variables availble for calling

these are not settable but can be called directly without refencing an external key.

```

unit_id - 001, 01a, 002, 02a etc.

internal_prefix - derived from the rfc1918_supernet defined in the group/hub_vpn under hub_sites.

vlan20v4 - derived from the v20prefix_v4 defined in the group/hub_vpn under hub_sites.
vlan21v4 - derived from the v21prefix_v4 defined in the group/hub_vpn under hub_sites.

v6supernet - derived from the v6_supernet defined in the group/hub_vpn under hub_sites.

loopback_v4 - derived from the v4loopback_addr above.
loopback_v6 - derived from the v6loopback_addr above.

netflow - ip address derived from netflow_exporter above.

manage_def_v4 - derived from the management_default_v4 above.
manage_def_v6 - derived from the management_default_v6 above

public_v4 - derived from the public_v4 above.
public_v6 - derived from the public_v6 above.

vlan200v4 - derived from the vlan200v4 above.
vlan200v6 - derived from the vlan200v6 above.

vlan20fe80 - derived from the vlan20fe80 above.
vlan21fe80 - derived from the vlan21fe80 above.
vlan200fe80 - derived from the vlan200fe80 above.

icrbgpv4_peer01 - derived from the icrbgpv4_peer01 above.
icrbgpv4_peer02 - derived from the icrbgpv4_peer02 above.

icrbgpv6_peer01 - derived from the icrbgpv6_peer01 above.
icrbgpv6_peer02 - derived from the icrbgpv6_peer02 above.

ecrbgpv4_peer01 - derived from the ecrbgpv4_peer01 above.
ecrbgpv4_peer02 - derived from the ecrbgpv4_peer02 above.

ecrbgpv6_peer01 - derived from the ecrbgpv6_peer01 above.
ecrbgpv6_peer02 - derived from the ecrbgpv6_peer02 above.

v4tuple_list[] - a list containing each tuple of the internal rfc1918 v4 supernet based upon the internal_prefix variable from above.
v6tuple_list[] - a list containing each tuple of the ipv6 supernet based upon v6supernet above

vlan20tuple_list[] - a list containing each tuple of the vlan20 inet4 network based upon vlan20v4 above.
vlan21tuple_list[] - a list containing each tuple of the vlan21 inet4 network based upon vlan21v4 above.



```
